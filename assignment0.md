# CPSC 470/570: Artificial Intelligence <br/> Homework 0 (warmup, 2 points)
Introduction to Python3

Due Wednesday, January 23, 11:59:59 PM

## I. Introduction
This assignment will introduce you to Python3 and give you a background needed for the class and further assignments.

## II. Work environment

While you should be able to work on your local machine, we encourage you to work on a Zoo machine as all the environment requirements (e.g. Python3) are set up for you. You can either work directly on a Zoo computer (on the 3rd floor of AKW and in Hillhouse 17) or work on your local machine and SHH into the Zoo. For more information about the cluster and how to SSH into the Zoo, visit http://zoo.cs.yale.edu/newzoo/.

Go to Canvas and download `assignment0.zip`. Unzip the folder and you should see the following files:
- animals.py
- animals.txt
- assignment0.py
- sample-solution.txt
- sample.txt

## III. Work with Python3
For this assignment, you have to complete 9 `TODO`'s from `assignment0.py`. We expect you to look up the documentation of Python3 to finish this assignment. The code should be relatively simple.
- `TODO 1, 2, 3, 4, 7` require you to insert 1 - 3 lines of code.
- `TODO 5, 6, 8, 9` require you to comment / uncomment one line.

Please read the file for specific instructions for each `TODO`.

The remaining files are:
- `animals.py`: defines the `Animal` class, which is used in `assignment0.py`.
- `animals.txt`: defines the *input* of the *private* test.
- `sample.txt`: defines the *input* of the *public* test.
- `sample_answers_solution.txt`: defines the *output* of the *public* test.

### 1. Test your implementation with public test
Finish `TODO 1, 2, 3, 4, 7` and leave the remaining `TODO`'s untouched.

Make your `assignment0.py` is executable by executing the following command:
```
chmod +x assignment0.py
```

Then, execute the script:
```
./assignment0.py
```

You should see a new file, `sample_answers.txt`, generated. If this file is identical to `sample_answers_solution.txt`, congratulations, you pass the public test! To test that the two files are identical, execute the following command:
```
diff sample_answers.txt sample_answers_solution.txt
```
You should see no output.

### 2. Generate the final solution for private test
Finish `TODO 5, 6, 8, 9`. Then, re-execute the script. You should see a new file, `answers.txt`, generated. This file will be compared with our solution as a private test.

## IV. Submit your assignment
- Make sure your assignment0 folder has at least 2 files:
  - `assignment0.py` with your implementation
  - `answers.txt`
- Zip your `assignment0` folder and name it `[netID]-assignment0.zip`.
- Upload the zip file to Canvas.

## V. Grading criteria
This assignment is worth two points.
- Public test: 1 point
- Private test: 1 point

## VI. Late policy
10% off for each day or part thereof. 
After three full days, the assignment will be given a score of zero.
